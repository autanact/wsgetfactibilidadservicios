#Bitacora de cambios servicio WsGetFactibilidadServicios
#Freddy Molina
#Fecha creaci�n:04/02/2016
#Descripi�n general: Servicio que se encarga de la cobertura (GPON, 3G, etc)
#�ltimas modificaci�nes
	03/07/2015 FMS Se crea WSDL e implementaci�n
	04/02/2016 FMS Se incorpora cobertora GPON. Solo est� hecho el WSDL, no se ha creado la clase. (Respaldo 2016/02/04)
	12/02/2016 FMS Se incorpora cobertora GPON. Se implementa la parte JAVA del GPON. (Respaldo 2016/02/12)
	17/02/2016 FMS Unificaci�n de servicio
#Versi�n actual
	12/02/2016 v1.0	PC/DESA/PRE/PROD
	17/02/2016 v2.0 PC/DESA