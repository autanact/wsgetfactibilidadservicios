
/**
 * WsGetFactibilidadServiciosSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsGetFactibilidadServiciosSkeleton java skeleton for the axisService
     */
    public class WsGetFactibilidadServiciosSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsGetFactibilidadServiciosLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param codigoDireccion
                                     * @param latitud
                                     * @param longitud
                                     * @param codigoMunicipio
         */
        

                 public co.net.une.www.gis.WsGetFactibilidadServiciosRSType getFactibilidadServicios
                  (
                  java.lang.String codigoSolicitud,java.lang.String codigoDireccion,java.lang.String latitud,java.lang.String longitud,java.lang.String codigoMunicipio
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("codigoDireccion",codigoDireccion);params.put("latitud",latitud);params.put("longitud",longitud);params.put("codigoMunicipio",codigoMunicipio);
		try{
		
			return (co.net.une.www.gis.WsGetFactibilidadServiciosRSType)
			this.makeStructuredRequest(serviceName, "getFactibilidadServicios", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    