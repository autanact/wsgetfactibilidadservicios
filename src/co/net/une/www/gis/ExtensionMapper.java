
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.gis;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsGetFactibilidadServicios-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsGetFactibilidadServiciosRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaFOType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaFOType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaTercerosType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaTercerosType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaPagoAnticipadoType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaPagoAnticipadoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "ListaProyectosType".equals(typeName)){
                   
                            return  co.net.une.www.gis.ListaProyectosType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "UTCDate".equals(typeName)){
                   
                            return  co.net.une.www.gis.UTCDate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaCobreType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaCobreType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString14".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString14.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsTerceroType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsTerceroType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "ProyectoFVType".equals(typeName)){
                   
                            return  co.net.une.www.gis.ProyectoFVType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaDTHType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaDTHType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCobertura3GSMType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCobertura3GSMType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaWimaxType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaWimaxType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsListaTercerosType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsListaTercerosType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaHFCType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaHFCType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaRitelType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaRitelType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "CoberturaGponType".equals(typeName)){
                   
                            return  co.net.une.www.gis.CoberturaGponType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsGetFactibilidadServicios-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsGetFactibilidadServiciosRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "GisRespuestaGeneralType".equals(typeName)){
                   
                            return  co.net.une.www.gis.GisRespuestaGeneralType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "wsCoberturaLTEType".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsCoberturaLTEType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    