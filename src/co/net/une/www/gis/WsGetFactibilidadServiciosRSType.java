
/**
 * WsGetFactibilidadServiciosRSType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.gis;
            

            /**
            *  WsGetFactibilidadServiciosRSType bean class
            */
        
        public  class WsGetFactibilidadServiciosRSType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = wsGetFactibilidadServicios-RS-Type
                Namespace URI = http://www.une.net.co/gis
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/gis")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for CodigoSolicitud
                        */

                        
                                    protected java.lang.String localCodigoSolicitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCodigoSolicitud(){
                               return localCodigoSolicitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoSolicitud
                               */
                               public void setCodigoSolicitud(java.lang.String param){
                            
                                            this.localCodigoSolicitud=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDireccion
                        */

                        
                                    protected java.lang.String localCodigoDireccion ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCodigoDireccion(){
                               return localCodigoDireccion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDireccion
                               */
                               public void setCodigoDireccion(java.lang.String param){
                            
                                            this.localCodigoDireccion=param;
                                    

                               }
                            

                        /**
                        * field for ZonaAltoRiesgo
                        */

                        
                                    protected java.lang.String localZonaAltoRiesgo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getZonaAltoRiesgo(){
                               return localZonaAltoRiesgo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ZonaAltoRiesgo
                               */
                               public void setZonaAltoRiesgo(java.lang.String param){
                            
                                            this.localZonaAltoRiesgo=param;
                                    

                               }
                            

                        /**
                        * field for EstadoZonaAltoRiesgo
                        */

                        
                                    protected java.lang.String localEstadoZonaAltoRiesgo ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEstadoZonaAltoRiesgo(){
                               return localEstadoZonaAltoRiesgo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstadoZonaAltoRiesgo
                               */
                               public void setEstadoZonaAltoRiesgo(java.lang.String param){
                            
                                            this.localEstadoZonaAltoRiesgo=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaGpon
                        */

                        
                                    protected co.net.une.www.gis.CoberturaGponType localCoberturaGpon ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.CoberturaGponType
                           */
                           public  co.net.une.www.gis.CoberturaGponType getCoberturaGpon(){
                               return localCoberturaGpon;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaGpon
                               */
                               public void setCoberturaGpon(co.net.une.www.gis.CoberturaGponType param){
                            
                                            this.localCoberturaGpon=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaHFC
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaHFCType localCoberturaHFC ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaHFCType
                           */
                           public  co.net.une.www.gis.WsCoberturaHFCType getCoberturaHFC(){
                               return localCoberturaHFC;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaHFC
                               */
                               public void setCoberturaHFC(co.net.une.www.gis.WsCoberturaHFCType param){
                            
                                            this.localCoberturaHFC=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaFO
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaFOType localCoberturaFO ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaFOType
                           */
                           public  co.net.une.www.gis.WsCoberturaFOType getCoberturaFO(){
                               return localCoberturaFO;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaFO
                               */
                               public void setCoberturaFO(co.net.une.www.gis.WsCoberturaFOType param){
                            
                                            this.localCoberturaFO=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaWimax
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaWimaxType localCoberturaWimax ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaWimaxType
                           */
                           public  co.net.une.www.gis.WsCoberturaWimaxType getCoberturaWimax(){
                               return localCoberturaWimax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaWimax
                               */
                               public void setCoberturaWimax(co.net.une.www.gis.WsCoberturaWimaxType param){
                            
                                            this.localCoberturaWimax=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaCobre
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaCobreType localCoberturaCobre ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaCobreType
                           */
                           public  co.net.une.www.gis.WsCoberturaCobreType getCoberturaCobre(){
                               return localCoberturaCobre;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaCobre
                               */
                               public void setCoberturaCobre(co.net.une.www.gis.WsCoberturaCobreType param){
                            
                                            this.localCoberturaCobre=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaLTE
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaLTEType localCoberturaLTE ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaLTEType
                           */
                           public  co.net.une.www.gis.WsCoberturaLTEType getCoberturaLTE(){
                               return localCoberturaLTE;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaLTE
                               */
                               public void setCoberturaLTE(co.net.une.www.gis.WsCoberturaLTEType param){
                            
                                            this.localCoberturaLTE=param;
                                    

                               }
                            

                        /**
                        * field for Cobertura3GSM
                        */

                        
                                    protected co.net.une.www.gis.WsCobertura3GSMType localCobertura3GSM ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCobertura3GSMType
                           */
                           public  co.net.une.www.gis.WsCobertura3GSMType getCobertura3GSM(){
                               return localCobertura3GSM;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cobertura3GSM
                               */
                               public void setCobertura3GSM(co.net.une.www.gis.WsCobertura3GSMType param){
                            
                                            this.localCobertura3GSM=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaDTH
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaDTHType localCoberturaDTH ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaDTHType
                           */
                           public  co.net.une.www.gis.WsCoberturaDTHType getCoberturaDTH(){
                               return localCoberturaDTH;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaDTH
                               */
                               public void setCoberturaDTH(co.net.une.www.gis.WsCoberturaDTHType param){
                            
                                            this.localCoberturaDTH=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaTerceros
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaTercerosType localCoberturaTerceros ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaTercerosType
                           */
                           public  co.net.une.www.gis.WsCoberturaTercerosType getCoberturaTerceros(){
                               return localCoberturaTerceros;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaTerceros
                               */
                               public void setCoberturaTerceros(co.net.une.www.gis.WsCoberturaTercerosType param){
                            
                                            this.localCoberturaTerceros=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaRitel
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaRitelType localCoberturaRitel ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaRitelType
                           */
                           public  co.net.une.www.gis.WsCoberturaRitelType getCoberturaRitel(){
                               return localCoberturaRitel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaRitel
                               */
                               public void setCoberturaRitel(co.net.une.www.gis.WsCoberturaRitelType param){
                            
                                            this.localCoberturaRitel=param;
                                    

                               }
                            

                        /**
                        * field for CoberturaPagoAnticipado
                        */

                        
                                    protected co.net.une.www.gis.WsCoberturaPagoAnticipadoType localCoberturaPagoAnticipado ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.WsCoberturaPagoAnticipadoType
                           */
                           public  co.net.une.www.gis.WsCoberturaPagoAnticipadoType getCoberturaPagoAnticipado(){
                               return localCoberturaPagoAnticipado;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoberturaPagoAnticipado
                               */
                               public void setCoberturaPagoAnticipado(co.net.une.www.gis.WsCoberturaPagoAnticipadoType param){
                            
                                            this.localCoberturaPagoAnticipado=param;
                                    

                               }
                            

                        /**
                        * field for GisRespuestaProceso
                        */

                        
                                    protected co.net.une.www.gis.GisRespuestaGeneralType localGisRespuestaProceso ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.GisRespuestaGeneralType
                           */
                           public  co.net.une.www.gis.GisRespuestaGeneralType getGisRespuestaProceso(){
                               return localGisRespuestaProceso;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GisRespuestaProceso
                               */
                               public void setGisRespuestaProceso(co.net.une.www.gis.GisRespuestaGeneralType param){
                            
                                            this.localGisRespuestaProceso=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       WsGetFactibilidadServiciosRSType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/gis");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":wsGetFactibilidadServicios-RS-Type",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "wsGetFactibilidadServicios-RS-Type",
                           xmlWriter);
                   }

               
                   }
               
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CodigoSolicitud", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CodigoSolicitud");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CodigoSolicitud");
                                    }
                                

                                          if (localCodigoSolicitud==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CodigoSolicitud cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCodigoSolicitud);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"CodigoDireccion", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"CodigoDireccion");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("CodigoDireccion");
                                    }
                                

                                          if (localCodigoDireccion==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("CodigoDireccion cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCodigoDireccion);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"ZonaAltoRiesgo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"ZonaAltoRiesgo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("ZonaAltoRiesgo");
                                    }
                                

                                          if (localZonaAltoRiesgo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ZonaAltoRiesgo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localZonaAltoRiesgo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"EstadoZonaAltoRiesgo", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"EstadoZonaAltoRiesgo");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("EstadoZonaAltoRiesgo");
                                    }
                                

                                          if (localEstadoZonaAltoRiesgo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("EstadoZonaAltoRiesgo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEstadoZonaAltoRiesgo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                            if (localCoberturaGpon==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaGpon cannot be null!!");
                                            }
                                           localCoberturaGpon.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaGpon"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaHFC==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaHFC cannot be null!!");
                                            }
                                           localCoberturaHFC.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaHFC"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaFO==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaFO cannot be null!!");
                                            }
                                           localCoberturaFO.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaFO"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaWimax==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaWimax cannot be null!!");
                                            }
                                           localCoberturaWimax.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaWimax"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaCobre==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaCobre cannot be null!!");
                                            }
                                           localCoberturaCobre.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaCobre"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaLTE==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaLTE cannot be null!!");
                                            }
                                           localCoberturaLTE.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaLTE"),
                                               factory,xmlWriter);
                                        
                                            if (localCobertura3GSM==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Cobertura3GSM cannot be null!!");
                                            }
                                           localCobertura3GSM.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Cobertura3GSM"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaDTH==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaDTH cannot be null!!");
                                            }
                                           localCoberturaDTH.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaDTH"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaTerceros==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaTerceros cannot be null!!");
                                            }
                                           localCoberturaTerceros.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaTerceros"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaRitel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaRitel cannot be null!!");
                                            }
                                           localCoberturaRitel.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaRitel"),
                                               factory,xmlWriter);
                                        
                                            if (localCoberturaPagoAnticipado==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CoberturaPagoAnticipado cannot be null!!");
                                            }
                                           localCoberturaPagoAnticipado.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaPagoAnticipado"),
                                               factory,xmlWriter);
                                        
                                            if (localGisRespuestaProceso==null){
                                                 throw new org.apache.axis2.databinding.ADBException("GisRespuestaProceso cannot be null!!");
                                            }
                                           localGisRespuestaProceso.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","GisRespuestaProceso"),
                                               factory,xmlWriter);
                                        
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoSolicitud"));
                                 
                                        if (localCodigoSolicitud != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodigoSolicitud));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CodigoSolicitud cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoDireccion"));
                                 
                                        if (localCodigoDireccion != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodigoDireccion));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("CodigoDireccion cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "ZonaAltoRiesgo"));
                                 
                                        if (localZonaAltoRiesgo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZonaAltoRiesgo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ZonaAltoRiesgo cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "EstadoZonaAltoRiesgo"));
                                 
                                        if (localEstadoZonaAltoRiesgo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstadoZonaAltoRiesgo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("EstadoZonaAltoRiesgo cannot be null!!");
                                        }
                                    
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaGpon"));
                            
                            
                                    if (localCoberturaGpon==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaGpon cannot be null!!");
                                    }
                                    elementList.add(localCoberturaGpon);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaHFC"));
                            
                            
                                    if (localCoberturaHFC==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaHFC cannot be null!!");
                                    }
                                    elementList.add(localCoberturaHFC);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaFO"));
                            
                            
                                    if (localCoberturaFO==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaFO cannot be null!!");
                                    }
                                    elementList.add(localCoberturaFO);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaWimax"));
                            
                            
                                    if (localCoberturaWimax==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaWimax cannot be null!!");
                                    }
                                    elementList.add(localCoberturaWimax);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaCobre"));
                            
                            
                                    if (localCoberturaCobre==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaCobre cannot be null!!");
                                    }
                                    elementList.add(localCoberturaCobre);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaLTE"));
                            
                            
                                    if (localCoberturaLTE==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaLTE cannot be null!!");
                                    }
                                    elementList.add(localCoberturaLTE);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Cobertura3GSM"));
                            
                            
                                    if (localCobertura3GSM==null){
                                         throw new org.apache.axis2.databinding.ADBException("Cobertura3GSM cannot be null!!");
                                    }
                                    elementList.add(localCobertura3GSM);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaDTH"));
                            
                            
                                    if (localCoberturaDTH==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaDTH cannot be null!!");
                                    }
                                    elementList.add(localCoberturaDTH);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaTerceros"));
                            
                            
                                    if (localCoberturaTerceros==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaTerceros cannot be null!!");
                                    }
                                    elementList.add(localCoberturaTerceros);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaRitel"));
                            
                            
                                    if (localCoberturaRitel==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaRitel cannot be null!!");
                                    }
                                    elementList.add(localCoberturaRitel);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CoberturaPagoAnticipado"));
                            
                            
                                    if (localCoberturaPagoAnticipado==null){
                                         throw new org.apache.axis2.databinding.ADBException("CoberturaPagoAnticipado cannot be null!!");
                                    }
                                    elementList.add(localCoberturaPagoAnticipado);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "GisRespuestaProceso"));
                            
                            
                                    if (localGisRespuestaProceso==null){
                                         throw new org.apache.axis2.databinding.ADBException("GisRespuestaProceso cannot be null!!");
                                    }
                                    elementList.add(localGisRespuestaProceso);
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WsGetFactibilidadServiciosRSType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WsGetFactibilidadServiciosRSType object =
                new WsGetFactibilidadServiciosRSType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"wsGetFactibilidadServicios-RS-Type".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsGetFactibilidadServiciosRSType)co.net.une.www.gis.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoSolicitud").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodigoSolicitud(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDireccion").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodigoDireccion(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","ZonaAltoRiesgo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZonaAltoRiesgo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","EstadoZonaAltoRiesgo").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstadoZonaAltoRiesgo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaGpon").equals(reader.getName())){
                                
                                                object.setCoberturaGpon(co.net.une.www.gis.CoberturaGponType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaHFC").equals(reader.getName())){
                                
                                                object.setCoberturaHFC(co.net.une.www.gis.WsCoberturaHFCType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaFO").equals(reader.getName())){
                                
                                                object.setCoberturaFO(co.net.une.www.gis.WsCoberturaFOType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaWimax").equals(reader.getName())){
                                
                                                object.setCoberturaWimax(co.net.une.www.gis.WsCoberturaWimaxType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaCobre").equals(reader.getName())){
                                
                                                object.setCoberturaCobre(co.net.une.www.gis.WsCoberturaCobreType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaLTE").equals(reader.getName())){
                                
                                                object.setCoberturaLTE(co.net.une.www.gis.WsCoberturaLTEType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Cobertura3GSM").equals(reader.getName())){
                                
                                                object.setCobertura3GSM(co.net.une.www.gis.WsCobertura3GSMType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaDTH").equals(reader.getName())){
                                
                                                object.setCoberturaDTH(co.net.une.www.gis.WsCoberturaDTHType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaTerceros").equals(reader.getName())){
                                
                                                object.setCoberturaTerceros(co.net.une.www.gis.WsCoberturaTercerosType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaRitel").equals(reader.getName())){
                                
                                                object.setCoberturaRitel(co.net.une.www.gis.WsCoberturaRitelType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CoberturaPagoAnticipado").equals(reader.getName())){
                                
                                                object.setCoberturaPagoAnticipado(co.net.une.www.gis.WsCoberturaPagoAnticipadoType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","GisRespuestaProceso").equals(reader.getName())){
                                
                                                object.setGisRespuestaProceso(co.net.une.www.gis.GisRespuestaGeneralType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          